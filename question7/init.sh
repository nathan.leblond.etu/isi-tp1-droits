#!/bin/bash
PWD=$(pwd)

# Création des groupes
addgroup groupe_a &> /dev/null
addgroup groupe_b &> /dev/null

#Création des utilisateurs pour les tests
adduser --ingroup groupe_a --disabled-password lambda_a1 <<'EOF'
a1
a1
a1
a1
a1
y
EOF
adduser --ingroup groupe_a --disabled-password lambda_a2 <<'EOF'
a2
a2
a2
a2
a2
y
EOF
adduser --ingroup groupe_b --disabled-password lambda_b1 <<'EOF'
b1
b1
b1
b1
b1
y
EOF
adduser --ingroup groupe_b --disabled-password lambda_b2 <<'EOF'
b2
b2
b2
b2
b2
y
EOF
adduser --ingroup root --disabled-password admin <<'EOF'
admin
admin
admin
admin
admin
y
EOF
usermod -a -G groupe_a,groupe_b admin &> /dev/null

#Création des directories
mkdir dir_{a,b,c}

#Mise en place des possesseurs
chown admin:groupe_a dir_a
chown admin:groupe_b dir_b
chown admin:root dir_c

#Mise en place des droits sur les dossiers
chmod 770 dir_{a,b}
chmod 771 dir_c #Possibilité de s'y rendre pour les membres des groupes a et b
chmod +t dir_{a,b}
chmod g+s dir_{a,b,c}

#Mise en place des droits par défault des fichiers nouvellement créés, permet aux autres membres des groupes de modifier les fichiers des autres (sauf renommage/suppression car sticky bit)
cd dir_a && umask 002 && echo "dir_a umask : " && umask && cd ..
cd dir_b && umask 002 && echo "dir_b umask : " && umask && cd ..

#Droits scripts de test
chown admin:root admin.sh && chmod 755 admin.sh && chmod u+s admin.sh
chown lambda_a1:groupe_a a.sh && chmod 755 a.sh && chmod u+s a.sh
chown lambda_b1:groupe_b b.sh && chmod 755 b.sh && chmod u+s b.sh

#Initialisation de certains fichiers
touch dir_a/file_a{1,2}
touch dir_a/file_admin
echo "Written by admin" >> dir_a/file_a1 && echo "Written by admin" >> dir_a/file_a2 && echo "Written by admin" >> dir_a/file_admin
chown lambda_a1 dir_a/file_a1
chown lambda_a2 dir_a/file_a2
chown admin dir_a/file_admin

touch dir_b/file_b{1,2}
echo "Written by admin" >> dir_b/file_b1 && echo "Written by admin" >> dir_b/file_b2 && echo "Written by admin" >> dir_b/file_admin
chown lambda_b1 dir_b/file_b1
chown lambda_b2 dir_b/file_b2
chown admin dir_b/file_admin

touch dir_c/file_admin && echo "Written by admin" >> dir_c/file_admin
chown admin dir_c/file_admin

#Lancement des exécutables de test
echo "#### Début des tests de lambda_a1 ####"
runuser -l lambda_a1 -c "$PWD/a.sh $PWD"

echo "#### Début des tests de lambda_b1 ####"
runuser -l lambda_b1 -c "$PWD/b.sh $PWD"

echo "#### Début des tests de admin ####"
runuser -l admin -c "$PWD/admin.sh $PWD"