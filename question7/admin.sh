#!/bin/bash

echo "admin, au rapport"
cd $1

#Accès à dir_a
cd dir_a && cd ../dir_b && cd ../dir_c && cd .. && echo "J'ai accès aux dossiers"

#Suppression fichier possédé
rm -f dir_a/file_admin && rm -f dir_b/file_admin && rm -f dir_c/file_admin && echo "Je peux supprimr ce que je souhaite"

#Création de fichier
touch dir_a/file_admin && touch dir_b/file_admin && touch dir_c/file_admin && echo "Je peux créer où je le souhaite"

#Modification fichier 
echo "Written by admin" >> dir_a/file_a1 && echo "Written by admin" >> dir_a/file_admin && echo "Je peux écrire dans les fichiers partagés A"
echo "Written by admin" >> dir_b/file_b1 && echo "Written by admin" >> dir_b/file_admin && echo "Je peux écrire dans les fichiers partagés B"
echo "Written by admin" >> dir_c/file_admin && echo "Je peux écrire dans les fichiers partagés C"

#Lecture fichiers
cat dir_a/file_a1 >> /dev/null && cat dir_a/file_admin >> /dev/null && echo "Je peux lire les fichiers partagés A"
cat dir_b/file_b1 >> /dev/null && cat dir_b/file_admin >> /dev/null && echo "Je peux lire les fichiers partagés B"
cat dir_c/file_admin >> /dev/null && echo "Je peux lire les fichiers partagés C"

#Renommage fichiers
mv dir_a/file_a1 dir_a/file_A1 && mv dir_a/file_admin dir_a/file_ADMIN && echo "Je peux renommer les fichiers partagés A"
mv dir_b/file_b1 dir_b/file_B1 && mv dir_b/file_admin dir_b/file_ADMIN && echo "Je peux renommer les fichiers partagés B"
mv dir_c/file_admin dir_c/file_ADMIN && echo "Je peux renommer les fichiers partagés C"


