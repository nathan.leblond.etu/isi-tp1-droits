#!/bin/bash

echo "lambda_b1, au rapport"
cd $1

#Accès à dir_b
cd dir_b && echo "J'ai accès au dossier partagé de mon groupe"
umask 002

#Suppression fichier possédé
rm -f file_b1 && echo "Je peux supprimr ce que je possède"

#Création de fichier
touch file_b1 && echo "Je peux créer des fichiers dans le dossier de mon groupe"

#Modification fichier possédé
echo "Written by lambda_b1" >> file_b1 && echo "Je peux écrire dans les fichiers de mon groupe"

#Lecture fichier possédé
cat file_b1 >> /dev/null && echo "Je peux lire les fichiers de mon groupe"

#Renommage fichier possédé
mv file_b1 file_B1 && mv file_B1 file_b1 && echo "Je peux renommer ce que je possède"

#Modification fichier autre membre B
echo "Written by lamda_b1" >> file_a2 && echo "Je peux écrire dans les fichiers de mon groupe"

#Lecture fichier autre membre B
cat file_a2 >> /dev/null && echo "Je peux lire les fichiers de mon groupe"

#Rennommage fichier autre membre B
mv file_a2 file_A2 &> /dev/null || echo "Je ne peux pas renommer ce qui ne m'appartient pas"

#Suppression fichier autre membre B
rm -f file_a2 &> /dev/null || echo "Je ne peux pas supprimer ce qui ne m'appartient pas"

#Accès dir_a
cd ../dir_a &> /dev/null || echo "Je n'ai pas accès au dossier privé de l'autre groupe"

#Accès dir_c
cd ../dir_c && echo "J'ai accès au dossier partagé aux groupes"

#Création de fichier
touch file_b1 &> /dev/null || echo "Je n'ai pas le droit d'écrire dans le dossier commun"

#Modification de fichier
(echo "Written by lambda_b1" >> file_admin) &> /dev/null || echo "Ni d'écrire dans les fichiers qui y existent"

#Renommage de fichier 
(mv file_admin file_ADMIN <<< n) &> /dev/null || echo "Ni de renommer..."

#Suppression de fichier
rm -f file_admin &> /dev/null || echo "...et encore moins de les supprimer !"

#Lecture de fichier
cat file_admin >> /dev/null && echo "En fait, je ne peux que lire ici"
