# Rendu "Les droits d’accès dans les systèmes UNIX"

## Monome

- Nom, Prénom, email: Leblond Nathan <nathan.leblond.etu@univ-lille.fr>

## Question 1

Réponse: Puisque l'EUID de toto qui exécute le processus va correspondre à l'UID du propriétaire du fichier, c'est le premier triplet qui servira à déterminer les droits.  
Or, toto n'a pas le droit d'écrire dans ce fichier, bien qu'il le possède. Il ne sera donc pas en mesure d'écrire.  
En revanche, tout membre du groupe **ubuntu** autre que toto aura le droit. 
Ex : open() renvoie -1 quand toto exécute le processus (code d'erreur exact attentud EACCES aurait été 13...) et renvoie 3 (fd attribué à l'ouverture) pour l'utilisateur ubuntu présent dans le groupe **ubuntu**.   

## Question 2

Réponse: En retirant le droit d'execution sur le groupe **ubuntu**, toto n'est plus en mesure de se rendre dans le directory **mydir** dont le propriétaire est **ubuntu**. En revanche, **ubuntu** lui en est capable (droit 765), de même que les autres utilisateurs (j'ai conservé les droits par défaut pour ceux-ci).  
On comprend donc rapidement que le droit "d'exécution" pour un directory désigne le droit d'accéder à ce directory. 

En créant un fichier "test.txt" en tant qu'ubuntu dans ce directory, on est en mesure de le lister lorsque l'on passe en toto. En revanche, nous ne sommes pas en mesure de lire les permissions sur ce fichier (remplacés par des "?").  

## Question 3

Réponse : Lorsque l'on tente d'exécuter le programme suid en tant que toto avant d'avoir activer le flag, nous voyons que le euid/ruid sont 1001, ceux de toto. En plus de cela, nous ne sommes pas en mesure d'ouvrir le fichier data.txt situé dans mydata. Nous n'avions pas le droit d'exécution sur ce directory en tant que toto.  

Une fois le flag activé, le ruid est toujours 1001, celui de toto, en revanche le euid est 1000, celui d'ubuntu. Le flag a donc bien permi de faire en sorte d'exécuter suid en tant que l'utilisateur ubuntu. Ainsi, ubuntu ayant le droit d'accéder à data.txt, nous sommes bien en mesure d'ouvrir et d'arricher le contenu du fichier.   

## Question 4

Réponse : bien que le script *suid.py* ait le flag *set-user-id* placé pour **ubuntu** nous récupérons l'EUID de **toto** en exécutant ce programme en tant que **toto**. Il semble donc que le flag ne fonctionne pas comme on s'y attendrait en permettant de toujours s'exécuter en tant qu'**ubuntu**. Le script python n'étant pas le réel processus exécuté par notre machine, le +c%u ne change pas l'EUID. Il faudrait probablement faire cette modification directement sur le binaire python qui nous sert à exécuter ce script.   

## Question 5
Réponse : Nous voyons que la commande chfn sert, notamment, à apporter des modification à des informations concernant des utilisateurs, nécessitant potentiellement d'aller écrire dans le fichier /etc/password.  
Or, ce fichier ne présente des droits **d'écriture** que pour l'utilsateur root (propriétaire du fichier).  
Ainsi, à moins d'être root ou d'exécuter des commandes en son nom, nous ne pouvons éditer /etc/passwd.  
Le binaire chfn a les droits "-rwsr-xr-x", ce qui signifie que le set-user-id flag est placé sur root, propriétaire du binaire. Ainsi, l'appel à cette commande se fait en son nom, ce qui permet d'apporter des modifications à /etc/passwd. 

Avant modif chfn : toto:x:1001:1000::/home/toto:/bin/bash  
Après modif chfn : toto:x:1001:1000:,666,0123456789,9876543210:/home/toto:/bin/bash

Nous avons effectivement été en mesure de modifier un fichie pour lequel nous n'avions pas explicitement les droits d'écriture en passant par un binaire qui s'exécutait en le nom d'un utilisateur ayant ces droits.  

## Question 6

Réponse : Le fichier /etc/passwd ne contient pas les mots de passe car il est accessible aux utilisateurs afin de leur permettre de modifier leurs informations. En revanche, le fichier /etc/shadow lui n'est accessible qu'à root, c'est lui qui contient les hash des mots de passes des utilisateurs de manière crypté. /etc/passwd est ainsi disponible et modifiable tout en permettant la correspondance avec les informations cachées dans /etc/shadow.  

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

init.sh doit avoir à minima les droits 700 et appartenir à root:root
usage : 1) exécuter init.sh en tant que root dans le dossier question7  

## Question 8

Le programme et les scripts dans le repertoire *question8*.

init.sh doit avoir à minima les droits 700 et appartenir à root:root
usage : 1) exécuter init.sh en tant que root dans le dossier question7  

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








