#include "check_pass.h"
int check_pass(const char* given_pwd, uid_t ruid){
    // Opening the passwd file
	FILE * f; 
	f = fopen("/home/admin/passwd", "r");
	if(f == NULL) {
        perror("error for fopen on /home/admin/passwd");
        return -1;
    }

    // Reading file
	size_t size = 0;
	char * buffer_line = NULL;
	ssize_t read;
	int separation;
	char* id = malloc(6);
	while ((read = getline(&buffer_line, &size, f)) != -1){
		separation = end_id(buffer_line);
		if (separation == -1){
			continue;
		}
		strncpy(id, buffer_line, separation);

		if (atoi(id) == ruid){
			break;
		}
		
		memset(id, 0, strlen(id));
	}
    if (read == -1){
        printf("KO: The requesting user do not have a password set\n\n");
        return 1;
    }


	if(strncmp(given_pwd, buffer_line + end_id(buffer_line) + 2, strlen(given_pwd)) != 0){
		printf("KO: Wrong password\n\n");
		return -1;
	}


    return 0;
}

// Get the index of the ":" char which is the separator between id and password
// In : line of /home/admin/passwd
// Out : index of ":" if found, -1 in other case
int end_id(const char* line_info){
	int index = 0;

	while (line_info[index] != 0 && line_info[index] != ':'){
		index++;
	}

	if (line_info[index]	== 0){
		return -1;
	}
	
	return index;
}
