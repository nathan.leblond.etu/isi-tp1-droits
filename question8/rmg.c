#include "check_pass.h"
#include <sys/types.h>
#include <stdbool.h> 
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#define SIZE_PWD 128
#define SIZE_GROUP_LIST 128

int main(int argc, char *argv[]) {
	// Check usage
	if (argc != 2){
		printf("usage: ./rmg path/to/file/to/remove\n");
		exit(EXIT_FAILURE);
	}
	
	struct stat statbuf;
	gid_t owner_gid;


	gid_t rgids[SIZE_GROUP_LIST];
	int nb_groups = getgroups(SIZE_GROUP_LIST, rgids);
	uid_t ruid = getuid();
	
	// Récupération gid du fichier avec stat
	if (stat(argv[1], &statbuf) != 0) {
		perror("Error for stat on given file");
		exit(EXIT_FAILURE);
	}
	owner_gid = statbuf.st_gid;

	// Check groupes
	bool belongs = false;
	int i;
	for(i = 0; i < nb_groups ; i++){
		belongs = belongs || (rgids[i] == owner_gid);
	}
	if (!belongs){
		printf("KO: You need to be part of the file's group you seek to remove\n\n");
		exit(EXIT_FAILURE);
	}
	

	// Get input password
	char buffer_pwd[SIZE_PWD];

	printf("password: ");
    fflush(stdout);
    scanf("%[^\n]", buffer_pwd);
	fgetc(stdin);	
	printf("\n");
	// Check if correct password was given
    if (check_pass(buffer_pwd, ruid) != 0){
		exit(EXIT_FAILURE);
    }

	assert(setuid(ruid) == 0);
    
	int returned_value = remove(argv[1]);
	if (returned_value != 0){
		perror("KO: Error for remove on given file");
		printf("\n");
		exit(EXIT_FAILURE);
    }
	printf("OK: Successfully removed %s\n\n",argv[1]);
	return 0;
}
