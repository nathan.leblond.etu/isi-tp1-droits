#!/bin/bash
PWD=$(pwd)

# Création des groupes
addgroup groupe_a &> /dev/null
addgroup groupe_b &> /dev/null

#Création des utilisateurs pour les tests
adduser --ingroup groupe_a --disabled-password lambda_a1 <<'EOF'
a1
a1
a1
a1
a1
y
EOF
adduser --ingroup groupe_a --disabled-password lambda_a2 <<'EOF'
a2
a2
a2
a2
a2
y
EOF
adduser --ingroup groupe_b --disabled-password lambda_b1 <<'EOF'
b1
b1
b1
b1
b1
y
EOF
adduser --ingroup groupe_b --disabled-password lambda_b2 <<'EOF'
b2
b2
b2
b2
b2
y
EOF
adduser --ingroup root --disabled-password admin <<'EOF'
admin
admin
admin
admin
admin
y
EOF
usermod -a -G groupe_a,groupe_b admin &> /dev/null

#Création des directories
mkdir dir_{a,b,c}

#Mise en place des possesseurs
chown admin:groupe_a dir_a
chown admin:groupe_b dir_b
chown admin:root dir_c

#Mise en place des droits sur les dossiers
chmod 770 dir_{a,b}
chmod 771 dir_c #Possibilité de s'y rendre pour les membres des groupes a et b
chmod +t dir_{a,b}
chmod g+s dir_{a,b,c}

#Mise en place des droits par défault des fichiers nouvelles créés, permet aux autres membres des groupes de modifier les fichiers des autres (sauf renommage/suppression car sticky bit)
cd dir_a && umask 002 && echo "dir_a umask : " && umask && cd ..
cd dir_b && umask 002 && echo "dir_b umask : " && umask && cd ..

#Initialisation de certains fichiers
touch dir_a/file_a{1,2}
touch dir_a/file_admin
echo "Written by admin" >> dir_a/file_a1 && echo "Written by admin" >> dir_a/file_a2 && echo "Written by admin" >> dir_a/file_admin
chown lambda_a1 dir_a/file_a1
chown lambda_a2 dir_a/file_a2
chown admin dir_a/file_admin

touch dir_b/file_b{1,2}
echo "Written by admin" >> dir_b/file_b1 && echo "Written by admin" >> dir_b/file_b2 && echo "Written by admin" >> dir_b/file_admin
chown lambda_b1 dir_b/file_b1
chown lambda_b2 dir_b/file_b2
chown admin dir_b/file_admin

touch dir_c/file_admin && echo "Written by admin" >> dir_c/file_admin
chown admin dir_c/file_admin

#Attribution des droits au programme rmg
chown admin:root rmg
chmod 711 rmg
chmod u+s rmg

#Ecriture du fichier de mdp
echo "1001: mdp_lambda_a1" > /home/admin/passwd
echo "1005: mdp_admin" >> /home/admin/passwd
chown admin:root /home/admin/passwd
chmod 600 /home/admin/passwd

#Tests

#Wrong Password
echo "Deleting file_admin of admin in dir_c as lambda_a1 with WRONG password, expects KO"
runuser -l lambda_a1 -c "($PWD/rmg  $PWD/dir_a/file_a1) <<< not_mdp_lambda_a1"

#Right password and file
echo "Deleting file_a1 of lambda_a1 in dir_a as lambda_a1, expects OK"
runuser -l lambda_a1 -c "($PWD/rmg  $PWD/dir_a/file_a1) <<< mdp_lambda_a1"

#Right password, wrong files
echo "Deleting file_a2 of lambda_a2 in dir_a as lambda_a1, expects KO"
runuser -l lambda_a1 -c "($PWD/rmg  $PWD/dir_a/file_a2) <<< mdp_lambda_a1"
echo "Deleting file_b1 of lambda_b1 in dir_b as lambda_a1, expects KO"
runuser -l lambda_a1 -c "($PWD/rmg  $PWD/dir_b/file_b1) <<< mdp_lambda_a1"
echo "Deleting file_admin of admin in dir_c as lambda_a1, expects KO"
runuser -l lambda_a1 -c "($PWD/rmg  $PWD/dir_c/file_admin) <<< mdp_lambda_a1"

#Right password and files
echo "Deleting file_a1 of lambda_a1 in dir_a as admin, expects OK"
runuser -l admin -c "($PWD/rmg  $PWD/dir_a/file_a2) <<< mdp_admin"
echo "Deleting file_b1 of lambda_b1 in dir_b as admin, expects OK"
runuser -l admin -c "($PWD/rmg  $PWD/dir_b/file_b1) <<< mdp_admin1"
echo "Deleting file_admin of admin in dir_c as admin, expects OK"
runuser -l admin -c "($PWD/rmg  $PWD/dir_c/file_admin) <<< mdp_admin"
