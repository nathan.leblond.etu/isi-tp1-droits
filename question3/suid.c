#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
int main(int argc, char *argv[]) {
	FILE *f;
	char buffer[1024];
	if (argc < 2) {
		printf("Missing argument\n");
		exit(EXIT_FAILURE);
	}

	printf("ruid : %d\n", getuid());
	printf("rgid : %d\n", getgid());
	printf("euid : %d\n", geteuid());
	printf("egid : %d\n", getegid());
	f = fopen(argv[1], "r");

	if (f == NULL) {
		printf("Cannot open file\n");
		exit(EXIT_FAILURE);
	}


	printf("File opens correctly\n");
	fread(&buffer, 1024, 1, f);
	printf("%s contains : %s", argv[1], buffer);
	return 0;
}
